This is a docker compose stack that implements an example of Kafka Connect
routing traffic to Splunk.  In order to use it, the Splunk connector must be
downloaded and extracted into the `connectors/` directory.  Additionally, the
connector image will come up, but you will need to manually add the connector,
examples are in `kafka-connect/create-sink-example.sh`.